package com.example.toandr.fototask;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;


public class MainActivity extends Activity{
    private Switch selfieSwitch;
    private TextView tvInfo;
    private Button btnExit;
    private SharedPreferences sharedPreferences;
    public static final String KEY_SHARED ="auto_foto";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        tvInfo = (TextView) findViewById(R.id.tvInfo);

        btnExit = (Button) findViewById(R.id.btnExit);

        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.exit(0);
            }
        });

        selfieSwitch = (Switch) findViewById(R.id.switchSelfie);
        selfieSwitch.setChecked(isSwitchOn());
        selfieSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){

                    tvInfo.setText(getString(R.string.on_switch_text));
                    saveSelfieSwitch(true);

                } else {

                    tvInfo.setText(getString(R.string.off_switch_text));
                    saveSelfieSwitch(false);
                }
            }
        });
    }

    private void saveSelfieSwitch(boolean switcher){
        sharedPreferences = getSharedPreferences(KEY_SHARED,MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(KEY_SHARED,switcher);
        editor.apply();
    }
    private boolean isSwitchOn(){
        sharedPreferences = getSharedPreferences(KEY_SHARED, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(KEY_SHARED,false);

        }


}
