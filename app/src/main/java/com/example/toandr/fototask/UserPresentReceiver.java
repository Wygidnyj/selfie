package com.example.toandr.fototask;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.provider.MediaStore;
import android.util.Log;

public class UserPresentReceiver extends BroadcastReceiver {
    private Context context;
    public UserPresentReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;
        // TODO: This method is called when the BroadcastReceiver is receiving
        if (isSwitchOn()){
            Log.d("AUTO_SELFIE", "receive user presence, make photo");
            startPhotoMaking();
        } else {
            Log.d("AUTO_SELFIE", "receive user presence, do nothing");
        }


    }
    private boolean isSwitchOn(){
        SharedPreferences sharedPreferences;
        sharedPreferences = context.getSharedPreferences(MainActivity.KEY_SHARED,Context.MODE_PRIVATE);
        boolean inOn = sharedPreferences.getBoolean(MainActivity.KEY_SHARED,false);
        return  inOn;

    }
    private void startPhotoMaking(){
        Intent intent = new Intent(context,CameraActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
       // intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);

    }





}
