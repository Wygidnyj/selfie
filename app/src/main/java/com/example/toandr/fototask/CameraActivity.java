package com.example.toandr.fototask;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import android.app.Activity;
import android.content.Intent;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;


public class CameraActivity extends Activity {
    private static Camera camera = null;
    private   File photoFile;
    private static final String TAG = "Camera";
    private String dir = "/Selfie";
    private final int ROTATION = 270;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        File pictures = Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES+dir);
        if (!pictures.isDirectory()){
            pictures.mkdirs();
        }

        photoFile = new File(pictures, getNameOfPhoto());
        try {
            makePhoto();
        }catch (Throwable e){
            Log.e(TAG, "Can not connect to camera");
            e.printStackTrace();
            exit();
        }

    }

    private void makePhoto(){
        cameraInit();
        camera.setPreviewCallback(null);
        camera.startPreview();
        Log.e(TAG, "Before onPictureTaken  ");
         camera.takePicture(null, null, new PictureCallback() {

             @Override
             public void onPictureTaken(byte[] data, Camera camera) {
                 try {
                     Log.e(TAG, "onPictureTaken");

                     FileOutputStream fos = new FileOutputStream(photoFile);
                     fos.write(data);
                     fos.close();

                     refreshGallery();
                 } catch (Exception e) { e.printStackTrace();
                 } finally { exit(); }
             }

         });

    }
    private void cameraInit(){
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        int cameraCount = Camera.getNumberOfCameras();

        for (int camIdx = 0; camIdx < cameraCount; camIdx++) {
            Camera.getCameraInfo(camIdx, cameraInfo);

            if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                try {
                    camera = Camera.open(camIdx);
                    Log.d(TAG,"Camera opened");

                    Camera.Parameters parameters = camera.getParameters();
                    parameters.setRotation(ROTATION);
                    camera.setParameters(parameters);
                } catch (Exception e){
                     e.printStackTrace();
                    exit();
                }

            }
        }
    }
    private String getNameOfPhoto(){

        Calendar c = Calendar.getInstance();
        String dateFormat = "ddMMyyyyHHmmss";

        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        return "S"+ sdf.format(c.getTime())+".jpg";

    }
    private void exit(){
        Log.e(TAG, "EXIT ");
        if (camera != null) { camera.release(); }

        camera = null;
        finish();
    }
    private void refreshGallery(){
        sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(photoFile)));
    }

}